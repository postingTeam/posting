﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contract = ConnectedExpress.Data.Post;
using System.ServiceModel.Web;
using System.ServiceModel;

namespace ConnectedExpress.Business.Post.Base
{
    public class PostGeneralJournal
    {
        private string _connectionString;
        
        public PostGeneralJournal(string connectionString)
        {
            _connectionString = connectionString;
        }

        public int PostJournal(List<Contract.AccountingJournal> accountingJournalHeader,List<Contract.AccountingJournalDetail> accountingJournalDetail)
        {
            var outputResponse = WebOperationContext.Current.OutgoingResponse;
            using (var context = new Contract.EntityModel(_connectionString))
            try
            {
                context.Add(accountingJournalHeader.Select(a => new Contract.AccountingJournal
                    {
                        JournalAmount = a.JournalAmount,
                        CurrencyCode = a.CurrencyCode,
                        DateCreated = a.DateCreated,
                        ExchangeRate = a.ExchangeRate,
                        DateModified = a.DateModified,
                        FiscalPeriod = a.FiscalPeriod,
                        IsAdjustment = a.IsAdjustment,
                        FiscalYear = a.FiscalYear,
                        JournalDate = a.JournalDate,
                        JournalAmountRate = a.JournalAmountRate,
                        IsReversing = a.IsReversing,
                        JournalRefCode = a.JournalRefCode,
                        JournalReference = a.JournalReference,
                        JournalReference2 = a.JournalReference2,
                        JournalType = a.JournalType,
                        UserCreated = a.UserCreated,
                        UserModified = a.UserModified
                    }));

                context.Add(accountingJournalDetail.Select(a => new Contract.AccountingJournalDetail
                    {
                        AccountCode = a.AccountCode,
                        AccountDescription = a.AccountDescription,
                        AccountGroup = a.AccountGroup,
                        BillCode = a.BillCode,
                        CountryCode = a.CountryCode,
                        Credit = a.Credit,
                        CreditRate = a.CreditRate,
                        CurrencyCode = a.CurrencyCode,
                        CustomerCode = a.CustomerCode,
                        DateCreated = a.DateCreated,
                        DateModified = a.DateModified,
                        Debit = a.Debit,
                        DebitRate = a.DebitRate,
                        DetailDescription = a.DetailDescription,
                        ExchangeRate = a.ExchangeRate,
                        IsCOGS = a.IsCOGS,
                        IsReverse = a.IsReverse,
                        ItemCode = a.ItemCode,
                        JournalCodeReference = a.JournalCodeReference,                        
                    }));
            }
            catch (Exception ex)
            {
                outputResponse.StatusCode = System.Net.HttpStatusCode.InternalServerError;
            }
            return 1;
        }
    }
}
