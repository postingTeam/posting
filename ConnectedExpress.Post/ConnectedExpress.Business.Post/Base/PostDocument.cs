﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConnectedExpress.Business.Post.Base
{
    public class PostDocument
    {
        string _connectionString;

        public PostDocument(string connectionString)
        {
            _connectionString = connectionString;
            //JOSHUA POGI
        }

        public bool Post(string documentCode, int postingCode, bool isRecap)
        {
            bool result = false;
            try
            {
                switch (postingCode)
                {
                    case 1:
                        result = PostCustomerInvoice(documentCode);
                        break;
                }


            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }
        public bool PostCustomerInvoice(string documentCode)
        {

           // throw new NotImplementedException();

            ConnectedExpress.Business.Post.Trading.Contact.PostCustomerInvoice facade = new ConnectedExpress.Business.Post.Trading.Contact.PostCustomerInvoice(_connectionString);
            
            return facade.PostInvoice(documentCode);
        }





    }
}
