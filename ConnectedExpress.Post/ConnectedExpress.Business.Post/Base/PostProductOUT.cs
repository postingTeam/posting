﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contract = ConnectedExpress.Data.Post;
using System.ServiceModel.Web;
using System.ServiceModel;

namespace ConnectedExpress.Business.Post.Base
{
    public class PostProductOUT
    {
        string _connectionString;
        public PostProductOUT(string connectionString)
        {
            _connectionString = connectionString;
            //'asdasdsa'
        }
        public int PostProductOut(List<Contract.InventoryCostHistory> inventoryCostHistory)
        {
            var outputResponse = WebOperationContext.Current.OutgoingResponse;
            using(var context = new Contract.EntityModel(_connectionString))
            {
                try
                { 
                    context.Add(inventoryCostHistory.Select(a => new Contract.InventoryCostHistory
                    {
                          PurchaseReceiptCode   = a.PurchaseReceiptCode     ,    
                          PurchaseReceiptDate   = a.PurchaseReceiptDate     ,
                          PurchaseReceiptLineNum= a.PurchaseReceiptLineNum  ,
                          Type                  = a.Type                    ,
                        //currencyCode          = a.currencyCode            ,
                        //exchangeRate          = a.exchangeRate            ,
                          BillCode              = a.BillCode                ,
                          BillDate              = a.BillDate                ,
                          BillLineNum           = a.BillLineNum             ,
                          SupplierCode          = a.SupplierCode            ,
                       // supplierName          = a.supplierName            ,
                          ItemCode              = a.ItemCode                ,
                          BatchSerialNum         = a.BatchSerialNum         ,
                          BatchSerialNumLineNum = a.BatchSerialNumLineNum   ,
                          WarehouseCode         = a.WarehouseCode           ,
                          UnconfirmedCost       = a.UnconfirmedCost         ,
                          UnconfirmedAvgCost    = a.UnconfirmedAvgCost      ,
                          UnconfirmedLandedCost = a.UnconfirmedLandedCost   ,
                          UnconfirmedLandedPerc = a.UnconfirmedLandedPerc   ,
                          ConfirmedCost         = a.ConfirmedCost           ,
                          ConfirmedAvgCost      = a.ConfirmedAvgCost        ,
                          ConfirmedLandedCost   = a.ConfirmedLandedCost     ,
                          ConfirmedLandedPerc   = a.ConfirmedLandedPerc     ,
                          OrigQty               = a.OrigQty                 ,
                          Qty                   = a.Qty                     ,
                          InStockQty            = a.InStockQty              
                    }));
                }
                catch(Exception ex)
                {
                    outputResponse.StatusCode = System.Net.HttpStatusCode.InternalServerError;
                }
            }
            return 1;
        }
    }
}
