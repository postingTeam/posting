﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model = ConnectedExpress.Data.Post;
using PostBase = ConnectedExpress.Business.Post.Base;


namespace ConnectedExpress.Business.Post.Trading.Contact
{
    public class PostCustomerInvoice
    {
        #region Variables
        string _connectionString;
        List<Model.CustomerInvoice> customerInvoiceList;
        List<Model.CustomerInvoiceDetail> customerInvoiceDetailList;
        List<Model.InventoryCostAllocation> inventoryCostAllocationList;
        List<Model.InventoryCostHistory> inventoryCostHistoryList;
        List<Model.TransactionTaxSummary> transactionTaxSummary;
        List<Model.AccountingJournal> accountingJournal;
        List<Model.AccountingJournalDetail> accountingJournalDetail;
        List<Model.DataContract.Base.TaxDetailModel> taxDetailList;
       
        #endregion


        public PostCustomerInvoice(string connectionString)
        {
            _connectionString = connectionString;
        }

        #region GetCustomerInvoiceList
        public List<Model.CustomerInvoice> GetCustomerInvoiceList(string documentCode)
        {
            using (var context = new Model.EntityModel(_connectionString))
            {
                customerInvoiceList = (from a in context.CustomerInvoices
                                       where a.InvoiceCode == documentCode && a.IsPosted == false
                                       select new Model.CustomerInvoice
                                       {
                                           InvoiceCode = a.InvoiceCode,
                                           SourceInvoiceCode = a.SourceInvoiceCode ?? string.Empty,
                                           Type = a.Type,
                                           InvoiceDate = a.InvoiceDate,
                                           ShippingDate = a.ShippingDate,
                                           DueDate = a.DueDate,
                                           CancelDate = a.CancelDate,
                                           BillToCode = a.BillToCode,
                                           BillToName = a.BillToName,
                                           ReceivableCode = a.ReceivableCode,
                                           FreightTaxCode = a.FreightTaxCode,
                                           Freight = a.Freight,
                                           FreightRate = a.FreightRate,
                                           FreightTax = a.FreightTax,
                                           FreightTaxRate = a.FreightTaxRate,
                                           Tax = a.Tax,
                                           TaxRate = a.TaxRate,
                                           OtherTaxCode = a.OtherTaxCode,
                                           Other = a.Other,
                                           OtherRate = a.OtherRate,
                                           Total = a.Total,
                                           TotalRate = a.TotalRate,
                                           Balance = a.Balance,
                                           BalanceRate = a.BalanceRate,
                                           ARAccountCode = a.ARAccountCode,
                                           FreightAccountCode = a.FreightAccountCode,
                                           DiscountAccountCode = a.DiscountAccountCode,
                                           OtherAccountCode = a.OtherAccountCode,
                                           CurrencyCode = a.CurrencyCode,
                                           ExchangeRate = a.ExchangeRate,
                                           IsBatch = a.IsBatch,
                                           IsPaid = a.IsPaid,
                                           IsPosted = a.IsPosted,
                                           IsAllowBackOrder = a.IsAllowBackOrder,
                                           CouponID = a.CouponID,
                                           CouponCode = a.CouponCode,
                                           CouponDiscount = a.CouponDiscount,
                                           CouponDiscountRate = a.CouponDiscountRate,
                                           POCode = a.POCode,
                                           SalesRepOrderCode = a.SalesRepOrderCode,
                                           WarehouseCode = a.WarehouseCode,
                                           ShipToCode = a.ShipToCode
                                       }).ToList();
            }

            return customerInvoiceList;
        }
        #endregion

        #region GetCustomerInvoiceDetailList
        public List<Model.CustomerInvoiceDetail> GetCustomerInvoiceDetailList(string documentCode)
        {
          
            using (var context = new Model.EntityModel(_connectionString))
            {
                customerInvoiceDetailList = (from a in context.CustomerInvoices
                                             join b in context.CustomerInvoiceDetails
                                             on a.InvoiceCode equals b.InvoiceCode
                                             where a.InvoiceCode == documentCode
                                             && b.ItemType != "Note"
                                             select new Model.CustomerInvoiceDetail
                                             {
                                                 InvoiceCode = a.InvoiceCode,
                                                 InvoiceDate = a.InvoiceDate,
                                                 SourceInvoiceCode = b.SourceInvoiceCode,
                                                 SourceLineNum = b.SourceLineNum,
                                                 DueDate = a.DueDate,
                                                 IsDropShip = b.IsDropShip,
                                                 ItemCode = b.ItemCode,
                                                 LineNum = b.LineNum,
                                                 WarehouseCode = b.WarehouseCode,
                                                 QuantityShipped =  b.QuantityShipped,
                                                 UnitMeasureQty = b.UnitMeasureQty,
                                                 SalesPrice =  b.SalesPrice,
                                                 SalesPriceRate = b.SalesPriceRate,
                                                 Cost = (b.Cost == 0 ? b.ActualCost : b.ActualCost),
                                                 CostRate = (b.CostRate == 0 ? b.ActualCostRate : b.ActualCostRate),
                                                 
                                                 ExtPrice = b.ExtPrice,
                                                 ExtPriceRate = b.ExtPriceRate,

                                                 TaxCode = b.TaxCode,
                                                 ItemType = b.ItemType,
                                                 Discount = b.Discount,
                                                 CouponDiscount = b.CouponDiscount,
                                                 CouponDiscountRate = b.CouponDiscountRate,
                                                 InventoryAccountCode =b.InventoryAccountCode,
                                                 SalesAccountCode = b.SalesAccountCode,
                                                 COGSAccountCode = b.COGSAccountCode,

                                                 IsBatch = a.IsBatch,
                                                 CustomerCode = a.BillToCode,
                                                 Type = a.Type

                                                

                                             }).ToList();
            }

            return customerInvoiceDetailList;
        }

        #endregion

        #region GetInventoryCostAllocation
        List<Model.InventoryCostAllocation> GetInventoryCostAllocation()
        {
            var ItemType = new List<string> { "Gift Card", "Gift Certificate", "NON-STOCK", "Electronic Download", "Service" };
           // var InventoryCostAllocation = new List<string>();
            using(var context = new Model.EntityModel(_connectionString))
            {
                var SumInvCostAllocation = (from a in customerInvoiceDetailList
                                        join b in context.InventoryItems
                                        on a.ItemCode equals b.ItemCode
                                        where !a.ItemType.Contains(ItemType.ToString())
                                        select new 
                                        {
                                            InvoiceCode = a.InvoiceCode,
                                            LineNum = a.LineNum,
                                            ItemCode = a.ItemCode,
                                            ExtUnconfirmedCost = b.AverageCost,
                                            totalQty = a.QuantityShipped, 
                                            cost = b.AverageCost,
                                            CostingMethod = b.CostingMethod,
                                            inventoryAccountCode = a.InventoryAccountCode,
                                            cogsAccountCode = a.COGSAccountCode,
                                            ItemName = b.ItemName,
                                            ItemType = b.ItemType
                                        }
                                         ).ToList();

                //update customerInvoiceDetail
                //customerInvoiceDetailList[0].ItemName = SumInvCostAllocation[0].ItemName;
                //customerInvoiceDetailList[0].CostingMethod = SumInvCostAllocation[0].CostingMethod;
                

                inventoryCostAllocationList = (from a in customerInvoiceDetailList
                                              join b in SumInvCostAllocation
                                              on a.InvoiceCode equals b.InvoiceCode into temp
                                              from d in
                                                  (from b in temp
                                                   where a.ItemCode == b.ItemCode
                                                   && a.LineNum == b.LineNum
                                                   select b).DefaultIfEmpty()
                                              where a.IsBatch == false
                                              select new Model.InventoryCostAllocation
                                          {
                                              SalesInvoiceCode = a.InvoiceCode,
                                              SalesInvoiceDate = a.InvoiceDate,
                                              CustomerCode = a.CustomerCode,
                                              SourceInvoiceCode = a.SourceInvoiceCode,
                                              SalesInvoiceLineNum = a.SourceLineNum,
                                              SalesInvoiceKitDetailLineNum = a.LineNum,
                                              ItemCode = a.ItemCode,
                                              BatchSerialNum = "1",
                                              WarehouseCode = a.WarehouseCode,
                                              //LocationCode = a.LocationCode,
                                              ExtPrice = a.SalesPrice / a.UnitMeasureQty,

                                              ExtUnconfirmedAvgCost = a.Cost / a.UnitMeasureQty,
                                              ExtUnconfirmedCost = a.Cost / a.UnitMeasureQty,
                                              ExtConfirmedAvgCost = a.Cost / a.UnitMeasureQty,
                                              ExtConfirmedCost = a.Cost / a.UnitMeasureQty,

                                              UnconfirmedLandedPerc = a.ExtPrice / a.QuantityShipped,
                                              ConfirmedLandedPerc = a.ExtPrice / a.QuantityShipped,
                                              ExtUnconfirmedLandedCost = a.ExtPrice / a.QuantityShipped,
                                              ExtConfirmedLandedCost = a.ExtPrice / a.QuantityShipped,

                                              Qty = a.QuantityShipped,
                                              CostingMethod = a.CostingMethod,
                                              Type = a.Type ?? "Invoice",
                                            
                                              Cost = d.cost
                                          }).ToList();

              return inventoryCostAllocationList;

            }
           
        }
        #endregion

        #region GetInventoryCostHistory
        List<Model.InventoryCostHistory> GetInventoryCostHistory()
        {
            var ItemType = new List<string> { "Gift Card", "Gift Certificate", "NON-STOCK", "Electronic Download", "Service" };
            inventoryCostHistoryList = (from a in customerInvoiceDetailList
                                        where a.QuantityShipped < 0 && !ItemType.Contains(ItemType.ToString())
                                        select new Model.InventoryCostHistory
                                        {
                                            PurchaseReceiptCode = a.InvoiceCode,
                                            PurchaseReceiptDate = a.InvoiceDate,
                                            PurchaseReceiptLineNum = a.LineNum,
                                            Type = a.Type,
                                            BillCode = a.InvoiceCode,
                                            BillDate = a.InvoiceDate,
                                            SupplierCode = a.CustomerCode,
                                            ItemCode = a.ItemCode,
                                            WarehouseCode = a.WarehouseCode,
                                            
                                            UnconfirmedCost = a.Cost,
                                            UnconfirmedAvgCost = a.Cost,

                                            UnconfirmedLandedCost = a.SalesPrice,
                                            UnconfirmedLandedPerc = 0,
                                            
                                            ConfirmedCost = a.Cost,
                                            ConfirmedAvgCost = a.Cost,

                                            ConfirmedLandedCost = a.SalesPrice,
                                            ConfirmedLandedPerc = 0,

                                            OrigQty = a.QuantityShipped,
                                            Qty = a.QuantityShipped,
                                            InStockQty = a.QuantityShipped,
                                            CostingMethod = a.CostingMethod,
                                        }
                                            ).ToList();

            return inventoryCostHistoryList;
        }



        #endregion

        #region GetTaxDetail
       public List<Model.DataContract.Base.TaxDetailModel> GetTaxDetail(string documentCode)
        {
            using (var context = new Model.EntityModel(_connectionString))
            {
                taxDetailList = (from a in context.TransactionTaxSummaries
                                         where a.DocumentCode == documentCode
                                         && (a.TaxAmount > 0 || a.TaxAmountRate > 0)
                                         select new Model.DataContract.Base.TaxDetailModel
                                         {
                                             DocumentCode = a.DocumentCode,
                                             Type = a.TransactionType,
                                             DocumentDate = a.TransactionDate,
                                             TaxCode = a.TaxCode,
                                             TaxCodeDetail = a.TaxCode,
                                             TaxAmount = a.TaxAmount,
                                             TaxableAmoutRate = a.TaxAmountRate,
                                             AccountCode = a.AccountCode,
                                         }).ToList();
            
               
               taxDetailList = (from a in customerInvoiceDetailList
                                where a.InvoiceCode == taxDetailList[0].DocumentCode
                                && a.ItemCode == taxDetailList[0].ItemCode
                                && a.LineNum == taxDetailList[0].LineNum
                                select new Model.DataContract.Base.TaxDetailModel
                                {
                                    WarehouseCode = a.WarehouseCode,
                                    SalesInvoiceCode = a.InvoiceCode,
                                    ItemName = a.ItemName
                                    
                                }).ToList();
            }

            return taxDetailList;
        }
        #endregion

        #region CreateAccountingJournal
        List<Model.AccountingJournal> CreateAccountingJournal(string documentCode)
        {
          
                accountingJournal = customerInvoiceList.Select(a => new Model.AccountingJournal
                {
                    JournalCode = a.InvoiceCode,
                    JournalReference = a.InvoiceCode,
                    JournalReference2 = a.CurrencyCode,
                    JournalDate = a.InvoiceDate,
                    JournalType = a.Type,
                    JournalAmount = a.Total,
                    JournalAmountRate = a.TotalRate,

                }
                    ).ToList();
           
            return accountingJournal;
        }

        #endregion

        #region CreateAccountingJournalDetail

        List<Model.AccountingJournalDetail> CreateAccountingJournalDetail(string documentCode)
        {
            #region ARJournalDetail
            var ARJournalDetail = (from cid in customerInvoiceDetailList
                                    join ci in customerInvoiceList
                                    on cid.InvoiceCode equals ci.InvoiceCode
                                    where (cid.QuantityShipped > 0)
                                    select new
                                    {
                                        ci.InvoiceCode,
                                        ci.InvoiceDate,
                                        ci.Type,
                                        ci.BillToCode,
                                        ci.ARAccountCode,
                                        DetailDesctiption = ci.InvoiceCode + "/" + ci.BillToCode + (ci.IsBatch == true ?
                                         "/Batch Invoice" + "/" + (ci.POCode ?? "/" + ci.SalesRepOrderCode) : ""),
                                        Debit = cid.ExtPrice,
                                        Credit = (decimal?)0,
                                        ci.CurrencyCode,
                                        ci.ExchangeRate,
                                        DebitRate = cid.ExtPriceRate,
                                        CreditRate = (decimal?)0
                                    }).Concat(
                                    
                                     //Negative(-)QUANTITYSHIPPED,FOR SILVERTOUCH PROJECT. 

                                   from cid in customerInvoiceDetailList
                                   join ci in customerInvoiceList
                                   on cid.InvoiceCode equals ci.InvoiceCode
                                   where (cid.QuantityShipped < 0)
                                   select new
                                   {
                                       ci.InvoiceCode,
                                       ci.InvoiceDate,
                                       ci.Type,
                                       ci.BillToCode,
                                       ci.ARAccountCode,
                                       DetailDesctiption = ci.InvoiceCode + "/" + ci.BillToCode + (ci.IsBatch == true ?
                                        "/Batch Invoice" + "/" + (ci.POCode ?? "/" + ci.SalesRepOrderCode) : ""),
                                       Debit = (decimal?) 0,     
                                       Credit = cid.ExtPrice * -1,
                                       ci.CurrencyCode,
                                       ci.ExchangeRate,
                                       DebitRate = (decimal?)0,
                                       CreditRate = cid.ExtPriceRate * -1
                                   }).Concat(
                                   
                                    //THIS IS USED WHEN THERE'S NO ITEM ON THE LINE BUT WE ADDED AMOUNT ON OTHER AND FREIGHT.  

                                    from ci in customerInvoiceList
                                    where !customerInvoiceDetailList.Any()
                                    select new
                                    {
                                        ci.InvoiceCode,
                                        ci.InvoiceDate,
                                        ci.Type,
                                        ci.BillToCode,
                                        ci.ARAccountCode,
                                        DetailDesctiption = ci.InvoiceCode + "/" + ci.BillToCode,
                                        Debit = (decimal?)0,
                                        Credit = (decimal?)0,
                                        ci.CurrencyCode,
                                        ci.ExchangeRate,
                                        DebitRate = (decimal?)0,
                                        CreditRate = (decimal?)0
                                    }).Concat(

                                     //THIS IS USED WHEN THERE'S ITEM ON THE LINE BUT WE ADDED AMOUNT ON OTHER AND FREIGHT.  

                                    from ci in customerInvoiceList
                                    where customerInvoiceDetailList.Where(asd => asd.QuantityShipped == 0).Any()
                                    select new
                                    {
                                        ci.InvoiceCode,
                                        ci.InvoiceDate,
                                        ci.Type,
                                        ci.BillToCode,
                                        ci.ARAccountCode,
                                        DetailDesctiption = ci.InvoiceCode + "/" + ci.BillToCode,
                                        Debit = (decimal?)0,
                                        Credit = (decimal?)0,
                                        ci.CurrencyCode,
                                        ci.ExchangeRate,
                                        DebitRate = (decimal?)0,
                                        CreditRate = (decimal?)0
                                    }).ToList();
            #endregion

            #region sumARJournalDetail
            var sumARJournalDetail = (from a in ARJournalDetail
                                      group a by new
                                      {
                                          a.InvoiceCode,
                                          a.InvoiceDate,
                                          a.Type,
                                          a.BillToCode,
                                          a.ARAccountCode,
                                          a.DetailDesctiption
                                      } into b
                                      select new 
                                      {
                                        InvoiceCode = b.Key.InvoiceCode,
                                        InvoiceDate= b.Key.InvoiceDate,
                                        Type = b.Key.Type,
                                        BillToCode = b.Key.BillToCode,
                                        ARAccountCode = b.Key.ARAccountCode,
                                        DetailDesctiption = b.Key.DetailDesctiption,
                                        Debit = b.Sum(s => s.Debit),
                                        Credit = b.Sum(s => s.Credit),
                                        DebitRate = b.Sum(s => s.DebitRate),
                                        CreditRate = b.Sum(s => s.CreditRate),                                         
                                      }).ToList();
            #endregion

            #region accountingJournalDetail
            accountingJournalDetail = (from a in sumARJournalDetail
                                       join b in customerInvoiceList
                                        on a.InvoiceCode equals b.InvoiceCode
                                       where ((a.Debit + b.Other + b.Freight + b.Tax) > 0)
                                       select new Model.AccountingJournalDetail 
                                       {
                                           JournalCode = a.InvoiceCode,
                                           JournalReference = a.InvoiceCode,
                                           JournalDate = a.InvoiceDate,
                                           JournalType = a.Type,
                                           LineReferenceCode1 = a.BillToCode,
                                           LineReferenceCode2 = null,
                                           LineReferenceCode3 = null,
                                           LineReferenceCode4 = null,
                                           CustomerCode = a.BillToCode,
                                           AccountCode = a.ARAccountCode,
                                           TaxType = "NA",
                                           TaxCode = null,
                                           ItemCode = null,
                                           WarehouseCode = null,
                                           SalesInvoiceCode = a.InvoiceCode,
                                           JournalCodeReference = a.InvoiceCode,
                                           JournalLineNumReference = null,
                                           DetailDescription = a.DetailDesctiption,
                                           Debit = a.Debit + b.Other + b.Freight + b.Tax,
                                           Credit = null,
                                           DebitRate = a.DebitRate + b.OtherRate + b.FreightRate + b.TaxRate,
                                           CreditRate = null,
                                           AccountLineType = "AR ACCOUNT CODE",
                                           IsCOGS = false,
                                           IsReverse = false
                                       }).Concat(

                                       from a in sumARJournalDetail
                                       where a.Credit != 0
                                       select new Model.AccountingJournalDetail
                                       {
                                           JournalCode = a.InvoiceCode,
                                           JournalReference = a.InvoiceCode,
                                           JournalDate = a.InvoiceDate,
                                           JournalType = a.Type,
                                           LineReferenceCode1 = a.BillToCode,
                                           LineReferenceCode2 = null,
                                           LineReferenceCode3 = null,
                                           LineReferenceCode4 = null,
                                           CustomerCode = a.BillToCode,
                                           AccountCode = a.ARAccountCode,
                                           TaxType = "NA",
                                           TaxCode = null,
                                           ItemCode = null,
                                           WarehouseCode = null,
                                           SalesInvoiceCode = a.InvoiceCode,
                                           JournalCodeReference = a.InvoiceCode,
                                           JournalLineNumReference = null,
                                           DetailDescription = a.DetailDesctiption,
                                           Debit = null,
                                           Credit = a.Credit,
                                           DebitRate = null,
                                           CreditRate = a.CreditRate,
                                           AccountLineType = "AR ACCOUNT CODE",
                                           IsCOGS = false,
                                           IsReverse = false
                                       }).Concat(


                                          from a in customerInvoiceList
                                          where a.Freight > 0
                                          select new Model.AccountingJournalDetail
                                          {
                                              JournalCode = a.InvoiceCode,
                                              JournalReference = a.InvoiceCode,
                                              JournalDate = a.InvoiceDate,
                                              JournalType = a.Type,
                                              LineReferenceCode1 = a.BillToCode,
                                              LineReferenceCode2 = null,
                                              LineReferenceCode3 = null,
                                              LineReferenceCode4 = null,
                                              CustomerCode = a.BillToCode,
                                              AccountCode = a.FreightAccountCode,
                                              TaxType = "Output Goods",
                                              TaxCode = a.FreightTaxCode,
                                              ItemCode = null,
                                              WarehouseCode = null,
                                              SalesInvoiceCode = a.InvoiceCode,
                                              JournalCodeReference = a.InvoiceCode,
                                              JournalLineNumReference = null,
                                              DetailDescription = a.InvoiceCode + "/" + a.BillToCode + "/Freight",
                                              Debit = null,
                                              Credit = a.Freight,
                                              DebitRate = null,
                                              CreditRate = a.FreightRate,
                                              AccountLineType = "FREIGHT ACCOUNT CODE",
                                              IsCOGS = false,
                                              IsReverse = false
                                          }).Concat(

          //var accountingJournalDetail4 = (from a in transactionTaxSummary
          //                                where a.TaxAmount > 0
          //                                select new Model.AccountingJournalDetail
          //                                {
          //                                    JournalCode = a.DocumentCode,
          //                                    JournalReference = a.DocumentCode,
          //                                    JournalDate = a.TransactionDate,
          //                                    JournalType = a.TransactionType,
          //                                    LineReferenceCode1 = a.en,
          //                                    LineReferenceCode2 = null,
          //                                    LineReferenceCode3 = null,
          //                                    LineReferenceCode4 = null,
          //                                    CustomerCode = a.BillToCode,
          //                                    AccountCode = a.FreightAccountCode,
          //                                    TaxType = "Output Goods",
          //                                    TaxCode = a.FreightTaxCode,
          //                                    ItemCode = null,
          //                                    WarehouseCode = null,
          //                                    SalesInvoiceCode = a.InvoiceCode,
          //                                    JournalCodeReference = a.InvoiceCode,
          //                                    JournalLineNumReference = null,
          //                                    DetailDescription = a.InvoiceCode + "/" + a.BillToCode + "/Freight",
          //                                    Debit = null,
          //                                    Credit = a.Freight,
          //                                    DebitRate = null,
          //                                    CreditRate = a.FreightRate,
          //                                    AccountLineType = a.acc,
          //                                    IsCOGS = false,
          //                                    IsReverse = false
          //                                }).ToList();


                                        from a in customerInvoiceDetailList
                                         where a.QuantityShipped != 0
                                          select new Model.AccountingJournalDetail
                                          {
                                              JournalCode = a.InvoiceCode,
                                              JournalReference = a.InvoiceCode,
                                              JournalDate = a.InvoiceDate,
                                              JournalType = a.Type,
                                              LineReferenceCode1 = a.CustomerCode,
                                              LineReferenceCode2 = a.InvoiceCode,
                                              LineReferenceCode3 = a.ItemCode,
                                              LineReferenceCode4 = a.WarehouseCode,
                                              CustomerCode = a.CustomerCode,
                                              AccountCode = a.SalesAccountCode,
                                              TaxType = "Output Goods",
                                              TaxCode = a.TaxCode,
                                              ItemCode = a.ItemCode,
                                              WarehouseCode = a.WarehouseCode,
                                              SalesInvoiceCode = a.InvoiceCode,
                                              JournalCodeReference = a.InvoiceCode,
                                              JournalLineNumReference = null,
                                              DetailDescription = a.InvoiceCode + "/" + a.CustomerCode + "/" + a.ItemName + "/Qty" + a.QuantityShipped + "@" + (a.ExtPrice / a.QuantityShipped) + "(SP)",
                                              Debit = null,
                                              Credit = a.ExtPrice,
                                              DebitRate = null,
                                              CreditRate = a.ExtPriceRate,
                                              AccountLineType = "SALES ACCOUNT CODE",
                                              IsCOGS = false,
                                              IsReverse = false
                                          }).Concat(

                                        from a in customerInvoiceList
                                         where a.Other > 0
                                         select new Model.AccountingJournalDetail
                                         {
                                             JournalCode = a.InvoiceCode,
                                             JournalReference = a.InvoiceCode,
                                             JournalDate = a.InvoiceDate,
                                             JournalType = a.Type,
                                             LineReferenceCode1 = a.BillToCode,
                                             LineReferenceCode2 = a.InvoiceCode,
                                             LineReferenceCode3 = null,
                                             LineReferenceCode4 = null,
                                             CustomerCode = a.BillToCode,
                                             AccountCode = a.OtherAccountCode,
                                             TaxType = "Output Goods",
                                             TaxCode = a.OtherTaxCode,
                                             ItemCode = null,
                                             WarehouseCode = null,
                                             SalesInvoiceCode = a.InvoiceCode,
                                             JournalCodeReference = a.InvoiceCode,
                                             JournalLineNumReference = null,
                                             DetailDescription = a.InvoiceCode + "/" + a.BillToCode + "/other",
                                             Debit = null,
                                             Credit = a.Other,
                                             DebitRate = null,
                                             CreditRate = a.Other,
                                             AccountLineType = "OTHER SALES ACCOUNT CODE",
                                             IsCOGS = false,
                                             IsReverse = false
                                         }).Concat(

                                          from a in inventoryCostAllocationList
                                          join b in customerInvoiceDetailList
                                          on new {p1=a.SalesInvoiceCode, p2=a.ItemCode, p3=a.SalesInvoiceLineNum} equals new {p1=b.InvoiceCode, p2=b.ItemCode, p3=b.LineNum}
                                          where a.ExtUnconfirmedAvgCost != 0
                                          select new Model.AccountingJournalDetail
                                          {
                                              JournalCode = b.InvoiceCode,
                                              JournalReference = b.InvoiceCode,
                                              JournalDate = b.InvoiceDate,
                                              JournalType = b.Type,
                                              LineReferenceCode1 = b.CustomerCode,
                                              LineReferenceCode2 = b.InvoiceCode,
                                              LineReferenceCode3 = b.ItemCode,
                                              LineReferenceCode4 = b.WarehouseCode,
                                              CustomerCode = b.CustomerCode,
                                              AccountCode = b.InventoryAccountCode,
                                              TaxType = "NA",
                                              TaxCode = "NA",
                                              ItemCode = b.ItemCode,
                                              WarehouseCode = b.WarehouseCode,
                                              SalesInvoiceCode = b.InvoiceCode,
                                              JournalCodeReference = b.InvoiceCode,
                                              JournalLineNumReference = null,
                                              DetailDescription = b.InvoiceCode + "/" + b.CustomerCode + b.ItemName + "/Qty" + b.QuantityShipped + "@" + b.Cost + "(CP)",
                                              Debit = a.ExtUnconfirmedAvgCost,
                                              Credit = null,
                                              DebitRate = a.ExtUnconfirmedAvgCost,
                                              CreditRate = null,
                                              AccountLineType = "COGS ACCOUNT CODE",
                                              IsCOGS = false,
                                              IsReverse = false
                                          }).Concat(


                                          from a in inventoryCostAllocationList
                                          join b in customerInvoiceDetailList
                                          on new {p1=a.SalesInvoiceCode, p2=a.ItemCode, p3=a.SalesInvoiceLineNum } equals new {p1=b.InvoiceCode, p2=b.ItemCode, p3=b.LineNum }
                                          where a.ExtUnconfirmedAvgCost != 0
                                          select new Model.AccountingJournalDetail
                                          {
                                              JournalCode = b.InvoiceCode,
                                              JournalReference = b.InvoiceCode,
                                              JournalDate = b.InvoiceDate,
                                              JournalType = b.Type,
                                              LineReferenceCode1 = b.CustomerCode,
                                              LineReferenceCode2 = b.InvoiceCode,
                                              LineReferenceCode3 = b.ItemCode,
                                              LineReferenceCode4 = b.WarehouseCode,
                                              CustomerCode = b.CustomerCode,
                                              AccountCode = b.InventoryAccountCode,
                                              TaxType = "NA",
                                              TaxCode = "NA",
                                              ItemCode = b.ItemCode,
                                              WarehouseCode = b.WarehouseCode,
                                              SalesInvoiceCode = b.InvoiceCode,
                                              JournalCodeReference = b.InvoiceCode,
                                              JournalLineNumReference = null,
                                              DetailDescription = b.InvoiceCode + "/" + b.CustomerCode + b.ItemName + "/Qty" + b.QuantityShipped + "@" + b.Cost + "(CP)",
                                              Debit = a.ExtUnconfirmedAvgCost,
                                              Credit = null,
                                              DebitRate = a.ExtUnconfirmedAvgCost,
                                              CreditRate = null,
                                              AccountLineType = "SALES ACCOUNT CODE",
                                              IsCOGS = false,
                                              IsReverse = false
                                          }).ToList();

            #endregion



            return accountingJournalDetail;
        }

        #endregion

        public bool PostInvoice(string documentCode)
        {

            customerInvoiceList = GetCustomerInvoiceList(documentCode);
            customerInvoiceDetailList = GetCustomerInvoiceDetailList(documentCode);
            inventoryCostAllocationList = GetInventoryCostAllocation();
            inventoryCostHistoryList = GetInventoryCostHistory();
            //taxDetailList = GetTaxDetail(documentCode);
            accountingJournal = CreateAccountingJournal(documentCode);
            accountingJournalDetail = CreateAccountingJournalDetail(documentCode);

            PostBase.PostGeneralJournal PostJournalFacade = new PostBase.PostGeneralJournal(_connectionString);
            PostJournalFacade.PostJournal(accountingJournal, accountingJournalDetail);
            //TESt
            return true;
        }
    }
}



