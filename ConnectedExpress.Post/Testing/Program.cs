﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConnectedExpress.Business.Post;
using System.Configuration;

namespace Testing
{
    class Program
    {
        static void Main(string[] args)
        {
            ConnectedExpress.Business.Post.Trading.Contact.PostCustomerInvoice facade = new ConnectedExpress.Business.Post.Trading.Contact.PostCustomerInvoice(ConfigurationManager.ConnectionStrings["Connection"].ConnectionString);

            facade.GetCustomerInvoiceList("INV-000050");
            facade.GetCustomerInvoiceDetailList("INV-000050");
            facade.GetTaxDetail("INV-000050");


        }
    }
}

