﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using ConnectedExpress.Business.Post.Base;
using System.Configuration;

namespace ConnectedExpress.Service.Post
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class PostService : IPostService
    {

        public int Post(string documentCode, int postingCode,bool isRecap)
        {
            ConnectedExpress.Business.Post.Base.PostDocument facade = new ConnectedExpress.Business.Post.Base.PostDocument(ConfigurationManager.ConnectionStrings["Connection"].ConnectionString);
            facade.Post(documentCode, postingCode,isRecap);
            throw new NotImplementedException();
        }
    }
}
