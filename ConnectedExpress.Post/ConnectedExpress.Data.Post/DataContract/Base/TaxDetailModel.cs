﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace ConnectedExpress.Data.Post.DataContract.Base
{
    [DataContract]
    public class TaxDetailModel
    {
        [DataMember]
       public string DocumentCode { get; set; }
        [DataMember]
        public int LineNum { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public DateTime? DocumentDate { get; set; }
        [DataMember]
        public DateTime? ShippingDate { get; set; }
        [DataMember]
        public DateTime? DateExpected { get; set; }
        [DataMember]
        public string EntityCode { get; set; }
        [DataMember]
        public string EntityName { get; set; }
        [DataMember]
        public string ProjectCode { get; set; }
        [DataMember]
        public string CustomerCode { get; set; }
        [DataMember]
        public string VendorCode { get; set; }
        [DataMember]
        public string ItemCode { get; set; }
        [DataMember]
        public string ItemName { get; set; }
        [DataMember]
        public string WarehouseCode { get; set; }
        [DataMember]
        public string LocationCode { get; set; }
        [DataMember]
        public string SalesInvoiceCode { get; set; }
        [DataMember]
        public string CreditMemoCode { get; set; }
        [DataMember]
        public string MACode { get; set; }
        [DataMember]
        public string ReceivableCode { get; set; }
        [DataMember]
        public string PurchaseReceiptCode { get; set; }
        [DataMember]
        public string BillCode { get; set; }
        [DataMember]
        public  string PayableCode { get; set; }
        [DataMember]
        public string DebitMemoCode { get; set; }
        [DataMember]
        public string RSCode { get; set; }
        [DataMember]
        public string TaxType { get; set; }
        [DataMember]
        public string TaxCode { get; set; }
        [DataMember]
        public string TaxCodeDetail { get; set; }
        [DataMember]
        public Decimal? TaxRate { get; set; }
        [DataMember]
        public  Decimal? TaxableAmoutRate { get; set; }
        [DataMember]
        public  Decimal? TaxAmount { get; set; }
        [DataMember]
        public Decimal? TaxAmoutRate { get; set; }
        [DataMember]
        public string AccountCode { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public  bool? IsHold { get; set; }
        [DataMember]
        public string AccountLineType { get; set; }


         

    }
}
