﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace ConnectedExpress.Data.Post.DataContract.Trading.Contact
{
    [DataContract]
    public class CustomerInvoiceDetailModel
    {
        [DataMember]
        public string InvoiceCode { get; set; }
        [DataMember]
        public string SourceInvoiceCode { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public int? SourceLineNum { get; set; }
        [DataMember]
        public DateTime? InvoiceDate { get; set; }
        [DataMember]
        public DateTime? ShippingDate { get; set; }
        [DataMember]
        public DateTime? DueDate { get; set; }
        [DataMember]
        public DateTime? CancelDate { get; set; }
        [DataMember]
        public string customerCode { get; set; }
        [DataMember]
        public string customername { get; set; }
        [DataMember]
        public string CurrencyCode { get; set; }
        [DataMember]
        public decimal? ExchangeRate { get; set; }
        [DataMember]
        public bool? IsBatch { get; set; }
        [DataMember]
        public bool? IsDropShip { get; set; }
        [DataMember]
        public string ItemCode { get; set; }
        [DataMember]
        public int LineNum { get; set; }
        [DataMember]
        public string itemKitDetailCode { get; set; }
        [DataMember]
        public string itemKitDetailLineNum { get; set; }
        [DataMember]
        public string WarehouseCode { get; set; }
        [DataMember]
        public decimal? quantityKitShipped { get; set; }
        [DataMember]
        public decimal? QuantityShipped { get; set; }
        [DataMember]
        public decimal? unitMeasureKitQty { get; set; }
        [DataMember]
        public decimal? unitMeasureQty { get; set; }
        [DataMember]
        public decimal? salesPrice { get; set; }
        [DataMember]
        public decimal? salesPriceRate { get; set; }
        [DataMember]
        public decimal? salesKitPrice { get; set; }
        [DataMember]
        public decimal? salesKitPriceRate { get; set; }
        [DataMember]
        public decimal? cost { get; set; }
        [DataMember]
        public decimal? costRate { get; set; }
        [DataMember]
        public decimal? extPrice { get; set; }
        [DataMember]
        public decimal? extPriceRate { get; set; }
        [DataMember]
        public decimal? extKitPrice { get; set; }
        [DataMember]
        public decimal? extKitPriceRate { get; set; }
        [DataMember]
        public string TaxCode { get; set; }
        [DataMember]
        public string ItemType { get; set; }
        [DataMember]
        public decimal? Discount { get; set; }
        [DataMember]
        public decimal? CouponDiscount { get; set; }
        [DataMember]
        public decimal? CouponDiscountRate { get; set; }
        [DataMember]
        public string inventoryAccountCode { get; set; }
        [DataMember]
        public string serviceAccountCode { get; set; }
        [DataMember]
        public string SalesAccountCode { get; set; }
        [DataMember]
        public string cogsAccountCode { get; set; }
        [DataMember]
        public string POCode { get; set; }
        [DataMember]
        public string SalesRepOrderCode { get; set; }
        [DataMember]
        public decimal? totalQty { get; set; }
        [DataMember]
        public string LocationCode { get; set; }
        [DataMember]
        public decimal? BatchSerialNum { get; set; }
    }
}
