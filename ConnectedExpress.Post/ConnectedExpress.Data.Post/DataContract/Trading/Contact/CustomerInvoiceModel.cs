﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace ConnectedExpress.Data.Post.DataContract.Trading.Contact
{
    [DataContract]
    public class CustomerInvoiceModel
    {
        [DataMember]
        public string InvoiceCode { get; set; }
        [DataMember]
        public string SourceInvoiceCode { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public DateTime? InvoiceDate { get; set; }
        [DataMember]
        public DateTime? ShippingDate { get; set; }
        [DataMember]
        public DateTime? DueDate { get; set; }
        [DataMember]
        public DateTime? CancelDate { get; set; }
        [DataMember]
        public string CustomerCode { get; set; }
        [DataMember]
        public string CustomerName { get; set; }
        [DataMember]
        public string ReceivableCode { get; set; }
        [DataMember]
        public string FreightTaxCode { get; set; }
        [DataMember]
        public decimal? Freight { get; set; }
        [DataMember]
        public decimal? FreightRate { get; set; }
        [DataMember]
        public decimal? FreightTax { get; set; }
        [DataMember]
        public decimal? FreightTaxRate { get; set; }
        [DataMember]
        public decimal? Tax { get; set; }
        [DataMember]
        public decimal? TaxRate { get; set; }
        [DataMember]
        public string OtherTaxCode { get; set; }
        [DataMember]
        public decimal? Other { get; set; }
        [DataMember]
        public decimal? OtherRate { get; set; }
        [DataMember]
        public decimal? Total { get; set; }
        [DataMember]
        public decimal? TotalRate { get; set; }
        [DataMember]
        public decimal? Balance { get; set; }
        [DataMember]
        public decimal? BalanceRate { get; set; }
        [DataMember]
        public string ARAccountCode { get; set; }
        [DataMember]
        public string FreightAccountCode { get; set; }
        [DataMember]
        public string DiscountAccountCode { get; set; }
        [DataMember]
        public string OtherAccountCode { get; set; }
        [DataMember]
        public string CurrencyCode { get; set; }
        [DataMember]
        public decimal? ExchangeRate { get; set; }
        [DataMember]
        public bool? IsBatch { get; set; }
        [DataMember]
        public bool? isPaid { get; set; }
        [DataMember]
        public bool? IsPosted { get; set; }
        [DataMember]
        public bool? IsAllowBackOrder { get; set; }
        [DataMember]
        public string CouponID { get; set; }
        [DataMember]
        public string CouponCode { get; set; }
        [DataMember]
        public decimal? CouponDiscount { get; set; }
        [DataMember]
        public decimal? CouponDiscountRate { get; set; }
        [DataMember]
        public string PoCode { get; set; }
        [DataMember]
        public string SalesRepOrderCode { get; set; }
        [DataMember]
        public string WarehouseCode { get; set; }
        [DataMember]
        public string ShipToCode { get; set; }
    }
}
